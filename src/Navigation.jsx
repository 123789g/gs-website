import React from "react";
import { NavLink } from "react-router-dom";


export default function Navigation() {
  return (
    <div>
      <NavLink to="/">Home</NavLink>
      <NavLink to="/About">About</NavLink>
      <NavLink to="/Projects">Projects</NavLink>
      <NavLink to="/Source">Source</NavLink>
      <NavLink to="/Contact">Contact</NavLink>
    </div>
  );
}