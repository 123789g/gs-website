
import './App.css';
import About from "./components/About"
import Contact from "./components/Contact"
import Projects from "./components/Projects"
import Error from "./components/Error"
import Home from "./components/Home"
import NavBar from "./components/NavBar"
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <NavBar  />
      <Routes> 
        <Route path='/' element={<Home/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/projects' element={<Projects/>}/>
        <Route path='/contact' element={<Contact/>}/>
        <Route path='/error' element={<Error/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
