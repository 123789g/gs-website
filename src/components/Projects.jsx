import React from "react";

export default function Projects() {
  return (
    <div>
      <div>
        <h1 className="Paragraph-Container">BlinkTBI</h1>
        <h3 className="Paragraph-Container">Angular - TypeScript - SCSS</h3>
        <p className="Paragraph">BlinkTBI has a machine (that I don't know the name of) that's main purpose is to blow a puff of air into your eye, and monitor how many times you blink after the puff of air. Using this data, doctors are able to predict whether the user has or soon will have a concussion.</p>

        <p className="Paragraph">My work on this project was building the dashboard that allowed doctors to interface with the data in a readable way. That means graphs that track the trends of particulr clients, dockets for medical records, tools to edit data in the back-end that determines how those graphs are drawn, and later in the project's life, I oversaw the transition of data in the application to go from athlete-centric to anyone-centric. BlinkTBI was the first time I was really let loose all on my lonesome and the project really is my baby. The amount of times I've sworn to re-factor it all in React are the same as when I swore that working on it was actually fun.</p>
      </div>
      <div>
        <h1 className="Paragraph-Container">ATI</h1>
        <h3 className="Paragraph-Container">React - Redux - MaterialUI</h3>
        <p className="Paragraph">ATI is a platform for too many things honestly. It livestreams, it tracks an astonishing amount of data that I don't understand, it has multiple Admin Dashboards. I worked on this project for about 6 months and I almost have no clue what I did. I'm sure everyone has a project like that. Where it feels like you put a serious amount of time into it and, like a black hole, it all disappears into nothing. Although my time with ATI was unremarkable, it was the first time I was allowed to touch a full-scale application in React. Almost immediatly after I finished my internship they threw me into the maw of this thing and I didn't sink. That's worth something I think. I built up the pop-up widget system and fixed the buttons routing to broken functions.</p>
      </div>
      <div>
        <h1 className="Paragraph-Container">Adesk</h1>
        <h3 className="Paragraph-Container">React - Redux - MaterialUI</h3>
        <p className="Paragraph">Adesk, which has recently rebranded to Crew Mama (is that like Go Daddy?????) is a dashboard built to schedule film shoots and gather up crews. It's probably the biggest application I've had my hands on and it's attatched to some serious big names like CNBC, HGTV, ESPN, Aljazeera, Complex and WAY more. ATI was big, don't get me wrong, but this was insane. The data that I had to parse through was obscene. And when I first started working on it, it took over 3 minutes just to load the page showing a list of crews. I put it lower on my list than ATI because, although it was the biggest app I've ever worked on, I didn't actually contribute anything to it that seemed impactful. By that I mean a lot of my work on this project was UI Tweaking. And fixing broken components like the search bar. I was being slapped onto a LOT of projects at the time, one of them in PHP, and I didn't get to really dive into Adesk the way I would've liked.</p>
      </div>
      <div>
        <h1 className="Paragraph-Container">Codeandtrust</h1>
        <h3 className="Paragraph-Container"> Pre-Refactor | Angular - TypeScript - SCSS</h3>
        <h3 className="Paragraph-Container"> Post-Refactor | React - Redux - MaterialUI</h3>
        <p className="Paragraph">This is the website for my employer. The company's site. When I was brought on as an intern, this site was going through it's refactor and I was put in the passenger seat for it. This site is a dashboard for employees that allowed us to list and apply statuses to our various projects. It's since been depreciated for time tracking software.</p>
      </div>
      <div>
        <h3 className="Paragraph-Container">Extensions I ❤️</h3>
        <p className="Paragraph">Here's a small group of Extensions that I ALWAYS have in my VSCodium.</p>
        <h5 className="Paragraph">Auto Close Tag - Does what it says on the tin. Can be a pain sometimes when adding a tag that you missed and having a new tag pop up, but a quick backspace to fix that quirk is a small price to pay for something so useful.</h5>
        <h5 className="Paragraph">Beautify - My premier code formatter. What's the difference between this and Prettier? No clue, but I'm using it anyways!</h5>
        <h5 className="Paragraph">Color Highlight - I don't have hex colors memorized and you shouldn't either! It also featues a color picker so you can grab the exact shade of pale blue for your *insert brand name here* logo without going to google!</h5>
        <h5 className="Paragraph">HTMLTagWrap - This is a hotkey (Alt + W by default) that will wrap whatever text you have selected in an opening tag and closing tag, then you can just type in *div* for example and it will input that *div* into those tags. </h5>
        <h5 className="Paragraph">LTeX - This is a spell-checker. Built for txt files mainly. Somehow, I've gone my life without this.</h5>
        <h5 className="Paragraph">Material Icon Theme - My go-to theme for VSCode and it's many branches. One day I'll make my own Icon Pack with pixel art but for now, this is a permanant extention for me.</h5>
      </div>
    </div>
  );
}
