import React from "react";
import hello from "../img/Hello.png";
import { useEffect, useState } from "react";

export default function Home() {
  const [state, setState] = useState("");
  useEffect(() => {
    fetch("https://programming-quotesapi.vercel.app/api/random")
      .then((response) => response.json())
      .then((quote) => setState(quote))
  }, []);

  return (
    <div>
      <div className="Banner-Container">
        <img src={hello} className="Hello"/>
        <h1>Hello World!</h1>
        <img src={hello} className="Hello"/>
      </div>
      <div className="Banner-Container"><p><b>{state.quote}</b> -<i>{state.author}</i></p></div>
      <div><p className="Paragraph">This is a website built for the purpose of showcasing myself. The code for the development of this website is located in it's <a href="https://gitlab.com/123789g/gs-website">repository</a>. Above this line is actually a random motivational quote that is fetched via Restful API. If for some reason it's not displaying, it's because I hit the fetch limit. I think it's about 100 every hour.
      </p></div>
    </div>

  );
}
