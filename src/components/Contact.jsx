import React from "react";


export default function Contact() {
  return (
    <div>
      <h1 className="Paragraph-Container">Contact Me</h1>
      <p className="Paragraph">I prefer all contact to be done by through direct messages on Indeed, LinkedIn or through my e-mail address <a href="mailto:galen.simmons0@gmail.com"><b>galen.simmons0@gmail.com</b></a>. I don't tend to answer my phone for unknown numbers unless I have an appointment set for your call.</p>
    </div>
  );
}
