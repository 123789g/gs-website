import React from "react";
import "../App.css";
import logo from "../img/Logo.png";
import { NavLink } from "react-router-dom";

export default function NavBar() {
  return (
    <div className="Nav-Container">
      <NavLink to="/">
        <img src={logo} className="Logo" />
      </NavLink>
      <div className="Routes">
        <NavLink to="/">Home</NavLink>
        <NavLink to="/about">About</NavLink>
        <NavLink to="/projects">Projects</NavLink>
        <NavLink to="/contact">Contact</NavLink>
      </div>
    </div>
  );
}
