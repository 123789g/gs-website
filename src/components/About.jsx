import React from "react";

export default function About() {
  return (
    <div>
      <div>
        <h1 className="Paragraph-Container">About Me</h1>
        <p className="Paragraph">
          Hey! I'm Galen Simmons, a <i>Software Engineer</i> originating from
          South Carolina. I've dipped my hands in a pool of industries but they
          all connect in one way or another to software so I typically use the
          title <i>Software Engineer</i> instead of <i>Front End Engineer</i>/
          <i>Technical Artist</i>/<i>Creative Director</i>/<i>3D Generalist</i>.
          My hobbies consist of making things (wood-working, soldering, machining,
          etc), playing an embarassing amount of videogames (don't ask how many),
          hiking, gushing over cars (2006 Impreza WRX STI 😩), studying interior
          design, and whatever happens to be interesting at the moment.
        </p>
      </div>

      <div>
        <h1 className="Paragraph-Container">Philosophy</h1>
        <p className="Paragraph">
          There's plenty of reasons why one should keep a set of internal
          standards when working on pretty much anything. It doesn't matter what
          the medium is, if you want to take pride in your work, then keeping up
          to snuff is a great place to start. But what is <i>Up to Snuff</i>?
          Here's a list of internal values that I try to hold when working on
          anything and everything.
        </p>
      </div>
      
      <div>
        <h2 className="Paragraph-Container">Be Flexible</h2>
        <p className="Paragraph">
          In software, what you learn very quickly upon working in out in the big
          bad world is the truth that very little software that you'd be working
          on, is written, by <b>you</b>, from scratch. Whenever collaborating on
          anything there's likely already a set of standards in place from your
          forebearers. Maybe the naming convention here is camelCase. Maybe they
          don't like having polycounts under 1k for simple props. Maybe they like
          to merge their changes every Sunday and you're not allowed to make
          changes on Saturday.
        </p>
      
      <p className="Paragraph">
        No matter the scenario, you're likely to find a workflow that doesn't
        fit what you were already doing. Instead of holding fast to your
        internal format or even worse, re-factoring the work that came before
        you, just go with the flow. Plenty of software out there, being used by
        millions, is written under-the-hood like a tornado came through. While you shouldn't
        strive to replicate the mess, you'd make yourself a far larger issue by
        going against the flow instead of with it.
      </p>
      <p className="Paragraph">
        Being flexible also gives you the grace to make mistakes. The grace to
        take those mistakes and turn them into opportunties to learn. You WILL
        make mistakes. You WILL screw up. You WILL fumble. But what's more
        important about failure is how you get back up and less about how you
        fall down. Even if you make the mistake a few times you have to keep in
        mind that with every mistake (or every angle to make the SAME mistake)
        comes the knowledge of how to avoid that in the future. Measure twice
        cut once. Simulate your material. Ask for advice from peers. Consult
        professionals. Make test pieces. Tear things apart and put them back
        together. And even after all that, when you screw up again, you can
        still get back up. To live is to learn. You will be living for quite a
        number of years! So learn well!
      </p>
      </div>

      <div>
        <h2 className="Paragraph-Container">K.I.S.S - Keep It Simple Stupid</h2>
        <p className="Paragraph">
          Where you can take the easy route, there's no harm in taking it.
          Innovation is the path less traveled. And there's a lot of pride one can
          take in finding a new solution to an old problem. I'm a fan of doing it
          myself, but the words left unsaid are;
          <i>
            The path less traveled sometimes doesn't have a path at all. It can be
            filled with thorns, snakes and terrible terrain.
          </i>
          It's just more practical to use the solution that you know works.
          Eventually the time will come where you NEED to make a new way forward.
          And the mindset that I use to tackle that is-
        </p>
      </div>

      <div>
        <h2 className="Paragraph-Container">Build Modularly</h2>
        <p className="Paragraph">
          This is a little different from Flexibility. Where flexibility is <b>reactive</b>, modularity is <b>proactive</b>. Build in pieces where
          it's reasonable to do so. Those pieces can be used by you for years to
          come in new ways that you would never think of. Even this website is
          actually using bits and pieces of my abandoned github projects. But a
          question that might pop up is <i>what makes something reasonable to make modular</i>? That's a
          difficult question and I'm still not committed to a full answer. But
          I'll give it a try.
        </p>
        <p className="Paragraph">
          Think about these pages for example. Could I <i>*inhale*</i> store all
          of this text in an array and pull it dynamically into a for loop that
          counts how many headers I have stored and then pairs the paragraphs with
          the headers allowing me to add text to the array which adds new bodys of
          text to the page? <i>*exhale*</i> Well, yes, but not only is that more complex than just
          writing it out, not only is it more difficult to read (digging through
          objects and arrays is never fun) the most important factor is <b>I don't want to! </b>
          That's right! I just don't wanna! Yes I COULD, but I won't! This weak
          justification is the reason why I don't have a committed answer. I mean,
          what good is this answer? A forloop would be able to generate content
          for every page on this site, yet I'm hard-coding it anyways! But hey,
          this is why I call these philosphies and not RULES. Hahaha!
        </p>
      </div>
    </div>
  );
}
